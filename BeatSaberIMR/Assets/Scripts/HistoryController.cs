using System.Collections;
using System.Collections.Generic;
using System.IO;
using TMPro;
using UnityEngine;

public class HistoryController : MonoBehaviour
{
    public Canvas login;
    public TMP_Text history;
    private string username;
    // Start is called before the first frame update
    void Start()
    {
        
        
       readData();  
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void updateData()
    {
        username = Settings.username;
        Debug.Log("Incerc sa fac history "+username+"b");
        readData();
    }
    private void readData()
    {

        history.text = "";
        FileInfo theSourceFile = new FileInfo("Assets/Resources/history.txt");
        StreamReader reader = theSourceFile.OpenText();

        string text;
        
        do
        {

            text = reader.ReadLine();
         
            if (text!=null&&text.StartsWith(username+";"))
            {
                text=text.Replace(";", "          ");
               history.text= text.Substring(username.Length + 4)+"\n"+history.text+text.Substring(username.Length+1);
               
            }

        } while (text != null);

        reader.Close();
       
    }
    
}
