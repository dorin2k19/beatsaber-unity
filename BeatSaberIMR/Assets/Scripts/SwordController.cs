using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

public class SwordController : MonoBehaviour
{
    public TMP_Text scoreText;
    public TMP_Text comboText;
    private int score = 0;
    private bool collide = false;
    public GameObject spawner;

    void Start()
    {
        scoreText.text = "Score:0";
        score = 0;
        
        
    }

    // Update is called once per frame
    void Update()
    {
        collide = false; 
    }
   void OnCollisionEnter(Collision collision)
    {
        
        if (collision.gameObject.tag == "cub"){
            collide = true;
            string[] combos = comboText.text.Split(':');
            int combo = Int32.Parse(combos[1].Substring(0,combos[1].Length-1));
            Debug.Log(combo);
            string[] words = scoreText.text.Split(':');
            score = Int32.Parse(words[1]);
            score += 10;
            scoreText.text = "Score:" + score;
        }
        else{            if (collision.gameObject.tag == "spikedBall" && !collide)
        {

           

            collide = true;
          /*  string[] words = scoreText.text.Split(':');
            score = Int32.Parse(words[1]);
            score -= 10;
            scoreText.text = "Score:" + score;*/
        }}
    }
    private void OnTriggerEnter(Collider other)
    {
        Debug.Log(other.gameObject.tag);
        
        if (other.gameObject.tag == "cub"&&!collide)
        {
            collide =true;
            Destroy(other.gameObject);
           
        }
        else
        {
            if (other.gameObject.tag == "head" && !collide) {
                if (Settings.speedGrowth<=7)
                Settings.speedGrowth ++;
                string[] combos = comboText.text.Split(':');
                int combo = Int32.Parse(combos[1].Substring(0, combos[1].Length - 1));
                if (combo <= 0) combo=1;
                

                Destroy(other.gameObject);
                string[] words = scoreText.text.Split(':');
                score = Int32.Parse(words[1]);
                score += combo*10;
                scoreText.text = "Score:" + score;
                combo++;
                comboText.text = "Combo:" + combo + "x";
                collide = true;
}

        }
        
            if (other.gameObject.tag == "spikedBall" && !collide)
            {


            comboText.text = "Combo:1x";
                collide = true;
                string[] words = scoreText.text.Split(':');
                score = Int32.Parse(words[1]);
           
                score -= 10;
            if (score < 0) score = 0;
                scoreText.text = "Score:" + score;
            Destroy(other.gameObject);
            }
        }

    }
    

