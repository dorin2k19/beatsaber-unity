using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeMover : MonoBehaviour
{
 
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {if (Settings.easy==1)
        transform.position += Time.deltaTime * transform.forward * 2;
        if (transform.position.x > 50) transform.position = new Vector3(35, transform.position.y, transform.position.z);
        else
        {
            int k = 2 + Settings.speedGrowth;
            transform.position += Time.deltaTime * transform.forward * k;

        }
    }
}
