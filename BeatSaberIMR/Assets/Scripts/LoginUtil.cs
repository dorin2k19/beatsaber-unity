using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;
using TMPro;

public class LoginUtil : MonoBehaviour
{
    string username = "";
    string password = "";
    public GameObject loginPage;
    public GameObject mainMenu;
    public GameObject inputUsername;
    public GameObject inputPassword;
    public GameObject invalid;
    public GameObject settings;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public string getUsername()
    {
        return username;
    }
    public void GetUsernameLogin(string s)
    {
        username = s;
    }

    public void GetPasswordLogin(string s)
    {
        password = s;
    }

    private bool ExistsUserPassword()
    {
        string path = "Assets/Resources/data.txt";

        FileInfo theSourceFile = new FileInfo("Assets/Resources/data.txt");
        StreamReader reader = theSourceFile.OpenText();

        string text;

        do
        {

            text = reader.ReadLine();
            Debug.Log(text);
            Debug.Log(username + ":" + password);
            if (text == username + ":" + password)
            {
                reader.Close();
                return true;
            }

        } while (text != null);

        reader.Close();
        return false;
    }

    public void Login()
    {   username= inputUsername.GetComponent<TMP_InputField>().text;
        password = inputPassword.GetComponent<TMP_InputField>().text;
        if (username == "" || password == "" || !ExistsUserPassword())
        {
            Debug.Log("Eroare la username sau parola!");
            invalid.SetActive(true);
        }
        else
        {
            Debug.Log("Logat cu succes!");
            mainMenu.SetActive(true);
            loginPage.SetActive(false);
            settings.GetComponent<Settings>().setUsername(username);
           
        }
    }

}
