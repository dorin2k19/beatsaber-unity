using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;
using TMPro;

public class RegisterUtil : MonoBehaviour
{
    string username = "";
    string password = "";
    public GameObject loginPage;
    public GameObject registerPage;
    public GameObject inputUsername;
    public GameObject inputPassword;
    public GameObject invalid;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private bool ExistsUsername()
    {
        string path = "Assets/Resources/data.txt";

        FileInfo theSourceFile = new FileInfo("Assets/Resources/data.txt");
        StreamReader reader = theSourceFile.OpenText();

        string text;

        do
        {

            text = reader.ReadLine();
            if (text != null)
            {
                var arr = text.Split(":"[0]);
                if (arr[0] == username)
                {
                    reader.Close();
                    return true;
                }
            }

        } while (text != null);

        reader.Close();
        return false;
    }

    public void Register()
    {
        username = inputUsername.GetComponent<TMP_InputField>().text;
        password = inputPassword.GetComponent<TMP_InputField>().text;
        if (username == "" || password == "" || ExistsUsername())
        {
            Debug.Log("Eroare la username sau parola!");
            invalid.SetActive(true);
        }
        else
        {
            Debug.Log("Registered!");
            // Write to file username:password
            string path = "Assets/Resources/data.txt";
            StreamWriter writer = new StreamWriter(path, true);
            writer.WriteLine(username + ":" + password);
            writer.Close();

            loginPage.SetActive(true);
            registerPage.SetActive(false);
        }
    }

    public void GetUsername(string s)
    {
        username = s;
    }

    public void GetPassword(string s)
    {
        password = s;
    }
}
