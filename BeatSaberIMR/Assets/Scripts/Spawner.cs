using System.Collections;
using System.Collections.Generic;
using System.IO;
using TMPro;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Spawner : MonoBehaviour
{
    public static bool ended = false;
    public TMP_Text timeText;
    public GameObject[] cubes;

    public Transform[] points;

    public float beat = (60 / 130) * 2;
    private float timer;

    private int timing;
    public TMP_Text score;
    private bool writted = false;
    private int startTime;

    // Start is called before the first frame update
    void Start()
    {
        startTime = (int)Time.time;


        if (SceneManager.sceneCount == 2)
        {
            SceneManager.SetActiveScene(SceneManager.GetSceneAt(1));
        }

    }


    // Update is called once per frame

    void writeHistory()
    {
        string path = "Assets/Resources/history.txt";
        StreamWriter writer = new StreamWriter(path, true);
        string dif;
        if (Settings.easy == 1) dif = "easy";
        else dif = "hard";
        writer.WriteLine(Settings.username + ";" + System.DateTime.Now.ToString() + ";" + dif + ";" + score.text.Split(':')[1]);
        writer.Close();
    }
    public void setEnded()
    {
        ended = !ended;
    }
    void Update()
    {

        timing = (Settings.timeLimit) - (int)Time.time + startTime;

        if (timing <= 0 && !writted)
        {
            writted = true;
            writeHistory();
            ended = true;


            string username = Settings.username;
            SceneManager.LoadScene(Settings.sceneNow.buildIndex);
            Settings.username = username;




        }
        setTime();
        if (timer > beat)
        {

            int poz = Random.Range(0, 16);
            if (poz % 5 == 0) poz = 2;
            else poz = poz % 2;
            int point = Random.Range(0, 8);
           GameObject cube =  Instantiate(cubes[poz], points[point]) as GameObject;
            cube.transform.localPosition = new Vector3(0, 0, 0);
            cube.transform.Rotate(transform.forward, 90 );
           
            if (poz != 2)
                cube.tag = "cub";
            else
                cube.tag = "spikedBall";
            timer -= beat;
        }

        timer += Time.deltaTime;
    }

    void setTime()
    {
        string hour, min;
        hour = "0" + ((timing / 60).ToString());
        if (timing % 60 < 10)
        {
            min = "0" + ((timing % 60).ToString());
        }
        else
        {
            min = (timing % 60).ToString();
        }
        timeText.text = hour + ":" + min;

    }

}
