using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeColor : MonoBehaviour
{
    public Color EstablishedColor;
    public Renderer SaberRenderer;
    // Start is called before the first frame update
    void Start()
    {SaberRenderer=gameObject.GetComponent<Renderer>();
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Y) && gameObject.CompareTag("sabie1"))
        {
            EstablishedColor = new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f),
                Random.Range(0f, 1f));
            SaberRenderer.material.color = EstablishedColor;
        }
        if (Input.GetKey(KeyCode.U) && gameObject.CompareTag("sabie2"))
        {EstablishedColor = new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f));
            SaberRenderer.material.color = EstablishedColor;
        }
    }
}
