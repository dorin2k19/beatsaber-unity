using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Settings : MonoBehaviour
{public static int easy=1,hard=0, timeLimit=30;
    public static string username="";
    public Slider timer;
    public TMP_Text timeNumber;
    public static Scene sceneNow;
    public static int speedGrowth = 0;
    public GameObject registerScene;
    public GameObject endScene;
    
  

    // Start is called before the first frame update
    void Start()
    {sceneNow=SceneManager.GetActiveScene();
       
        
    }
    
  
    
    void Update()
    {
        if (Spawner.ended)
        {
           Spawner.ended=false;
            
            endScene.SetActive(true);
            registerScene.SetActive(false);
            endScene.GetComponent<EndGameController>().updateScore(username);
        }
       
        
    }
    public void changeToEasyDifficulty()
    {easy=1;
        hard=0;}
    public void changToHardDifficulty()
    {easy=0;
    hard=1;}
    public void onValueChanged()
    {
        
        timeLimit = (int)(timer.value);
        timeNumber.text=timeLimit.ToString();
        Debug.Log(timeLimit);
    }
    public void setUsername(string username1)
    {username = username1;
    }
    public string getUsername() {
        return username;
    }
    public void loadGamee()
    {
       
        Scene baseScene=SceneManager.GetActiveScene();
       
        SceneManager.LoadScene("GameScene", LoadSceneMode.Additive);

        Debug.Log("Scene nr:" + SceneManager.sceneCount);

        


    }
    public void restartGame()
    {
        
        SceneManager.LoadScene(1, LoadSceneMode.Additive);
       
    }
}
