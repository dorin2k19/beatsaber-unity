using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class KeyboardController : MonoBehaviour
{
    // Start is called before the first frame update

    public GameObject inputUsername;
    public GameObject inputPassword;
    private bool inputSel=false;
    private bool passwordSel = false;
    private bool caps = false;
    void Start()
    {
       
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void setCaps()
    {
        caps = !caps;
    }
    public void addText(string text)
    {
        Debug.Log("Adauga");
        if (inputSel)
        {
          
            string text1 = inputUsername.GetComponent<TMP_InputField>().text;
            if (caps && "qwertyuiopasdfghjklzxcvbnm".Contains(text))
            {
                inputUsername.GetComponent<TMP_InputField>().text = text1 + text.ToUpper();
            }
            else
            {
                inputUsername.GetComponent<TMP_InputField>().text = text1 + text;
            }

        }
        else
        {
            if (passwordSel)
            {
                string text1 = inputPassword.GetComponent<TMP_InputField>().text;
                if (caps && "qwertyuiopasdfghjklzxcvbnm".Contains(text))
                {
                    inputPassword.GetComponent<TMP_InputField>().text = text1 + text.ToUpper();
                }
                else
                {
                    inputPassword.GetComponent<TMP_InputField>().text = text1 + text;
                }

            }
        }
    }
    public void deleteChar()
    {
        if (inputSel)
        {
            Debug.Log("delete");
            string text1 = inputUsername.GetComponent<TMP_InputField>().text;
            if (text1.Length > 0) {
                
                text1=text1.Remove(text1.Length - 1);
               
            }

            inputUsername.GetComponent<TMP_InputField>().text = text1;
        }
        else
        {
            if (passwordSel)
            {
                string text1 = inputPassword.GetComponent<TMP_InputField>().text;
                if (text1.Length > 0) text1.Remove(text1.Length - 1);
                inputPassword.GetComponent<TMP_InputField>().text = text1;
            }
        }
    }
    public void setUserSel()
    {
        Debug.Log("Selected");
        inputSel = true;
        passwordSel = false;
    }
   
    public void setPassSel()
    {
        passwordSel = true;
        inputSel = false;
    }
   
}
