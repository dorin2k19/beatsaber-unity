using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class WallController : MonoBehaviour
{
    public TMP_Text comboText;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "cub")
        {
            comboText.text = "Combo:1x";
            
        }
       
            Destroy(other.gameObject);
        
        
    }
}
