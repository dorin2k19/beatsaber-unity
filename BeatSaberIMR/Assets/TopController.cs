using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using TMPro;
using UnityEngine;

public class TopController : MonoBehaviour
{
    private Dictionary<string, int> top = new Dictionary<string, int>();
    public TMP_Text history;
    // Start is called before the first frame update
    void Start()
    {
        makeTop();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void printData()
    {

    }
    private void makeTop()
    {
        FileInfo theSourceFile = new FileInfo("Assets/Resources/history.txt");
        StreamReader reader = theSourceFile.OpenText();

        string text;
        
        do
        {

            text = reader.ReadLine();

            if (text != null )
            {
                int i = text.Length - 1;
                int score = 0;
                int j = 1;
                while (text[i] != ';')
                {
                    if ("0123456789".Contains(text[i]))
                    {
                        score = score + (text[i] - '0') * j;
                        j = j * 10;
                        i--;
                    }

                    
                }
                 j = 0;
                while (text[j] != ';') j++;
                string user=text.Substring(0,j);
                if (top.ContainsKey(user))
                {
                    if (top[user] < score)
                    {
                        top[user] = score;
                    }
                }
                else
                {
                    top.Add(user, score);
                }
            }

        } while (text != null);
     var topMax= from entry in top orderby entry.Value descending select entry;
        history.text = "";

        int pod = 1;
       foreach( var x in topMax)
        {
            history.text=history.text+"\n"+pod.ToString()+".  "+x.Key.ToString()+"  with : "+ x.Value.ToString();
            pod++;
        }
        reader.Close();
    }
}
