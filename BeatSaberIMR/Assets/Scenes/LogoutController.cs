using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LogoutController : MonoBehaviour
{
    public Canvas LoginReg;

    // Start is called before the first frame update
    void Start()
    {
        
    }
    public void Logout()
    {
        Settings.username = "";
        LoginReg.gameObject.SetActive(true);
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
