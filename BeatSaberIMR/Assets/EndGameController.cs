using System.Collections;
using System.Collections.Generic;
using System.IO;
using TMPro;
using UnityEngine;

public class EndGameController : MonoBehaviour
{
    public TMP_Text scoreText;
    // Start is called before the first frame update
    void Start()
    {
        
    }
    public void updateScore(string username)
    {
        FileInfo theSourceFile = new FileInfo("Assets/Resources/history.txt");
        StreamReader reader = theSourceFile.OpenText();

        string text;
        string score = "";
        do
        {

            text = reader.ReadLine();

            if (text != null && text.StartsWith(username))
            {
                var obj = text.Split(';');
                score=obj[obj.Length-1];
                
            
            }

        } while (text != null);
        scoreText.text = "Your score is: " + score;

        reader.Close();

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
